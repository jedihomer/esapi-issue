docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)

docker run -p 8888:8888 -d --name esapi-broken jedihomer/esapi:broken
docker run -p 8889:8888 -d --name esapi-works jedihomer/esapi:works
