docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)

docker build -f Dockerfile-broken -t jedihomer/esapi:broken .
docker build -f Dockerfile-works -t jedihomer/esapi:works .

docker push jedihomer/esapi:broken
docker push jedihomer/esapi:works
