# esapi-issue

Simple app to show

https://luceeserver.atlassian.net/browse/LDEV-2293

GitLab Project

https://gitlab.com/jedihomer/esapi-issue

Docker Images

https://hub.docker.com/r/jedihomer/esapi

Run the two images:

```sh
$ docker run -p 8888:8888 -d --name esapi-broken jedihomer/esapi:broken
$ docker run -p 8889:8888 -d --name esapi-works jedihomer/esapi:works
```

If you browse to `http://locahost:8888/` you will see an error, if you browse to `http://locahost:8889/` you will see a successful dump.
